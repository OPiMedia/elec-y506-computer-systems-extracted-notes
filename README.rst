.. -*- restructuredtext -*-

==============================================
Computer Systems — notes extracted from slides
==============================================
`PDF document`_ containing all notes extracted from the slides of the VUB course
`Computer Systems`_
(Jacques Tiberghien 2018-2019, VUB 4004876EER, ULB ELEC-Y506).

.. _`Computer Systems`: https://caliweb.cumulus.vub.ac.be/caliweb/?page=course-offer&id=001688&anchor=1&target=pr&language=en&output=html
.. _`PDF document`: https://bitbucket.org/OPiMedia/elec-y506-computer-systems-extracted-notes/raw/master/ELEC-Y506-Computer-Systems--Extracted-notes.pdf


If you want modify the document,
just modify the ``txt/ch*_notes.txt`` corresponding to the chapter
and run ``make`` to rebuild the PDF document.



Other materials around the master in computer science: http://www.opimedia.be/CV/2017-2018-ULB/

|



Author of the extraction from the slides: 🌳 Olivier Pirson — OPi |OPi| 🇧🇪🇫🇷🇬🇧 🐧 👨‍💻 👨‍🔬
====================================================================================================
🌐 Website: http://www.opimedia.be/

💾 Bitbucket: https://bitbucket.org/OPiMedia/

* 📧 olivier.pirson.opi@gmail.com
* Mastodon: https://mamot.fr/@OPiMedia — Twitter: https://twitter.com/OPirson
* 👨‍💻 LinkedIn: https://www.linkedin.com/in/olivierpirson/ — CV: http://www.opimedia.be/CV/English.html
* other profiles: http://www.opimedia.be/about/

.. |OPi| image:: http://www.opimedia.be/_png/OPi.png



|Computer Systems|

(image extracted from the course slides)

.. |Computer Systems| image:: https://bitbucket-assetroot.s3.amazonaws.com/c/photos/2020/Jun/14/520962127-1-elec-y506-computer-systems-extracted-n_avatar.png
